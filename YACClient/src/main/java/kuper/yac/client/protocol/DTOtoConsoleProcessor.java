package kuper.yac.client.protocol;

import kuper.yac.api.SimpleProcessor;
import java.io.Serializable;
import kuper.yac.api.dto.Confirmation;
import kuper.yac.api.dto.Message;
import kuper.yac.client.console.ConsoleImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vkuprin
 */
public class DTOtoConsoleProcessor extends SimpleProcessor {

    private final Logger logger = LoggerFactory.getLogger(
            DTOtoConsoleProcessor.class);

    @Override
    public void echo(Serializable object, Object... args) {
        if (object instanceof Message) {
            Message message = (Message) object;

            super.echo(message.getAuthor());
            super.echo(message.timestampString());
            super.echo(message.getMessage());
        } else if (object instanceof Confirmation) {
            Confirmation confirmation = (Confirmation) object;
            super.echo(confirmation.getState() + ": " + confirmation.getInfo());
        } else {
            logger.info("Unsupported object: {}", object.toString());
        }
    }

}
