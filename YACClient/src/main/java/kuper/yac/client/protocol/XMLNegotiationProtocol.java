package kuper.yac.client.protocol;

import kuper.yac.client.console.AuthDialog;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import javax.xml.stream.XMLStreamException;
import kuper.yac.api.XMLProtocol;
import kuper.yac.api.dto.AuthRequest;
import kuper.yac.api.dto.AuthResponse;
import kuper.yac.api.dto.Confirmation;
import kuper.yac.client.Manager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vkuprin
 */
public class XMLNegotiationProtocol extends XMLProtocol {

    private final Logger logger = LoggerFactory.getLogger(
            XMLNegotiationProtocol.class);

    private InputStream inputStream;
    private OutputStream outputStream;

    private AuthDialog authDialog;

    private Manager manager;

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    private enum States {
        GREETING, WAIT_AUTH, STOPPED
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public void setOutputStream(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public void setAuthDialog(AuthDialog authDialog) {
        this.authDialog = authDialog;
    }

    private States state = States.GREETING;

    @Override
    public void stop() {
        state = States.STOPPED;
        listen = false;
    }

    @Override
    public void process() {
        try {
            createReader();
            createWriter();

            writer.writeStartElement("root");

            listen();

            writer.writeEndElement();
            writer.close();
            reader.close();

        } catch (XMLStreamException ex) {
            logger.debug("Session closed");
            logger.trace(ex.getLocalizedMessage());
        }
    }

    @Override
    protected void processDTO(Serializable dto) {
        logger.trace("processing dto: {}", dto.toString());
        if (States.GREETING.equals(state) && (dto instanceof AuthRequest)) {
            AuthResponse authResponse;
            try {
                authResponse = authDialog.dialog();
                manager.setUserName(authResponse.getName());
                manager.setPasswd(authResponse.getName());
                logger.debug("sending Auth response for {}", authResponse.getName());
                encoder.encode(writer, authResponse);
                state = States.WAIT_AUTH;
            } catch (IOException ex) {
                logger.error("IO exception on Auth dialog: {}", ex.getMessage());
                state = States.STOPPED;
            }
        } else if (States.WAIT_AUTH.equals(state) && (dto instanceof Confirmation)) {
            Confirmation confirmation = (Confirmation) dto;
            if (Confirmation.State.SUCCESS.equals(confirmation.getState())) {
                Integer port = Integer.parseInt(confirmation.getInfo());
                logger.trace("port: {}", port);
                Confirmation answer = new Confirmation();
                if (establishSession(port)) {
                    answer.setState(Confirmation.State.CONFIRM);
                } else {
                    answer.setState(Confirmation.State.FAILURE);
                }
                encoder.encode(writer, answer);
                state = States.STOPPED;
            } else {
                state = States.STOPPED;
            }
        } else {
            logger.info("Got wrong dto: {}", dto.toString());
            state = States.STOPPED;
        }
        if (States.STOPPED.equals(state)) {
            listen = false;
        }
    }

    private boolean establishSession(final int port) {
        logger.debug("try esteblish connection for {}", manager.getUserName());
        SessionThread session = new SessionThread();
        session.setManager(manager);
        session.setName("Transport"+session.getName());
        manager.setSession(session);
        session.setAddress(manager.getAddress());
        session.setPort(port);
        session.start();
        while (session.isStarting()) {
            try {
                Thread.currentThread().sleep(100);
            } catch (InterruptedException ex) {
                logger.debug(ex.getLocalizedMessage());
            }
        }
        logger.debug("session for user {} is {}", manager.getUserName(), session.isRunning());
        return session.isRunning();
    }

    @Override
    public boolean isStarted() {
        return true;
    }

}
