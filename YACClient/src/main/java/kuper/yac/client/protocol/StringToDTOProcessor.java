package kuper.yac.client.protocol;

import kuper.yac.api.SimpleProcessor;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import kuper.yac.api.dto.Command;
import kuper.yac.api.dto.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vkuprin
 */
public class StringToDTOProcessor extends SimpleProcessor {

    private final Logger logger = LoggerFactory.getLogger(
            StringToDTOProcessor.class);

    @Override
    public void echo(Serializable object, Object... args) {
        if (object instanceof String) {
            String string = object.toString();
            if (string.trim().startsWith("\\")) {
                String com = string.substring(string.indexOf("\\") + 1).trim();
                Command command = new Command();
                int argumentsStart = com.indexOf(" ");
                if (argumentsStart > 0) {
                    command.setCommand(com.substring(0, argumentsStart));
                    command.setArguments(com.substring(argumentsStart).trim());
                } else {
                    command.setCommand(com);
                }
                super.echo(command);
            } else {
                Message message = new Message(null, string);
                super.echo(message);
            }
        } else {
            logger.info("Input must be String");
        }
    }

}
