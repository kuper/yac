package kuper.yac.client.protocol;

import kuper.yac.api.Processor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import javax.net.SocketFactory;
import kuper.yac.api.ApplicationContextProvider;
import kuper.yac.client.Manager;
import kuper.yac.client.console.Console;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author kuper
 */
public class SessionThread extends Thread {

    private final Logger logger = LoggerFactory.getLogger(SessionThread.class);
    
    private String address;
    private Integer port;
    private Manager manager;

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }
    
    private boolean init = true;
    
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
    private XMLSessionProtocol sessionProtocol;
    
    @Override
    public void run() {
        ApplicationContext ctx = ApplicationContextProvider.getContext();
        sessionProtocol = (XMLSessionProtocol) ctx.getBean(
                "sessionProtocol");
        try (Socket socket = SocketFactory.getDefault().createSocket(
                address,
                port);
             InputStream input = socket.getInputStream();
             OutputStream output = socket.getOutputStream();) {
            Console console = manager.getConsole();
            Processor dtoProcessor = (Processor) ctx.getBean("dtoToConsole");
            dtoProcessor.registerOutput(console);
            Processor stringProcessor = (Processor) ctx.getBean("consoleToDTO");
            console.registerOutput(stringProcessor);
            stringProcessor.registerOutput(sessionProtocol);
            sessionProtocol.registerOutput(dtoProcessor);
            sessionProtocol.setInput(input);
            sessionProtocol.setOutput(output);
            ((XMLSessionProtocol) sessionProtocol).setManager(manager);
            init = false;
            sessionProtocol.process();
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
        } finally {
            init = false;
            logger.trace("Session finished");
        }
    }
    
    public boolean isRunning() {
        return init || (null!=sessionProtocol && sessionProtocol.isActive());
    }
    
    public boolean isStarting() {
        return init || (null!=sessionProtocol && !sessionProtocol.isStarted());
        
    }

}
