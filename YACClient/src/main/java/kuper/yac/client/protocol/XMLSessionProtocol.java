package kuper.yac.client.protocol;

import kuper.yac.api.Processor;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.xml.stream.XMLStreamException;
import kuper.yac.api.XMLProtocol;
import kuper.yac.api.dto.AuthRequest;
import kuper.yac.api.dto.AuthResponse;
import kuper.yac.api.dto.Command;
import kuper.yac.api.dto.Confirmation;
import kuper.yac.api.dto.Message;
import kuper.yac.client.Manager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vkuprin
 */
public class XMLSessionProtocol extends XMLProtocol implements Processor {

    private final Logger logger = LoggerFactory.getLogger(XMLSessionProtocol.class);

    private Set<Processor> listeners = new HashSet<>();

    @Override
    public void registerOutput(Processor output) {
        listeners.add(output);
    }

    @Override
    public void unregisterOutput(Processor output) {
        listeners.remove(output);
    }

    private enum States {
        GREETING, WAIT_AUTH, CHATING, STOPPED
    }

    private Manager manager;

    private States state = States.GREETING;

    @Override
    public boolean isStarted() {
        return state.equals(States.CHATING) || state.equals(States.STOPPED);
    }

    @Override
    public boolean isActive() {
        return super.isActive() && !state.equals(States.STOPPED); 
    }

    @Override
    public void process() {
        try {
            createReader();
            createWriter();
            synchronized (writer) {
                writer.writeStartElement("root");
            }
            listen();
            synchronized (writer) {
                writer.writeEndElement();
                writer.close();
                reader.close();
            }
        } catch (XMLStreamException ex) {
            logger.debug("Session closed");
            logger.trace(ex.getLocalizedMessage());
        }
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    @Override
    public void stop() {
        listen = false;
        state = States.STOPPED;
    }

    @Override
    protected void processDTO(Serializable dto) {
        logger.trace("Processing dto: {}", dto.toString());
        if (States.GREETING.equals(state) && (dto instanceof AuthRequest)) {
            AuthResponse authResponse = new AuthResponse();
            authResponse.setName(manager.getUserName());
            authResponse.setPasswd(manager.getPasswd());
            encoder.encode(writer, authResponse);
            state = States.WAIT_AUTH;
        } else if (States.WAIT_AUTH.equals(state) && (dto instanceof Confirmation)) {
            Confirmation confirmation = (Confirmation) dto;
            if (Confirmation.State.SUCCESS.equals(confirmation.getState())) {
                state = States.CHATING;
                Command getHistory = new Command();
                getHistory.setCommand("history");
                echo(getHistory);
            } else {
                state = States.STOPPED;
            }
        } else if (States.CHATING.equals(state)) {
            for (Processor listener : listeners) {
                listener.echo(dto);
            }
        } else {
            logger.info("Got wrong dto: {}", dto.toString());
        }
        if (States.STOPPED.equals(state)) {
            listen = false;
        }
    }

    @Override
    public void echo(Serializable object, Object... args) {
        if (state.equals(States.CHATING)) {
            if (object instanceof Message) {
                ((Message) object).setAuthor(manager.getUserName());
            } else if (object instanceof Command) {
                ((Command) object).setAuthor(manager.getUserName());
            }
            encoder.encode(writer, object);
        }
    }

}
