package kuper.yac.client.console;

import java.io.IOException;
import kuper.yac.api.dto.AuthResponse;
import kuper.yac.client.console.ConsoleImpl;

/**
 *
 * @author kuper
 */
public class AuthDialogImpl implements AuthDialog {

    private ConsoleImpl console;

    public ConsoleImpl getConsole() {
        return console;
    }

    public void setConsole(ConsoleImpl console) {
        this.console = console;
    }

    @Override
    public AuthResponse dialog() throws IOException {

        console.echo("Enter your name");
        String userName = "";

        while (userName.isEmpty()) {
            userName = console.readln(null);
        }

        console.echo("Enter your passwd");
        String passwd;

        passwd = console.readln(null);

        AuthResponse authResponse = new AuthResponse();
        authResponse.setName(userName);
        authResponse.setPasswd(passwd);
        return authResponse;
    }

}
