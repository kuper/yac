package kuper.yac.client.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import kuper.yac.api.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vkuprin
 */
public class ConsoleImpl extends Console implements Processor {

    private final BufferedReader reader = new BufferedReader(
            new InputStreamReader(
                    System.in));

    
    private final Logger logger = LoggerFactory.getLogger(ConsoleImpl.class);

    
    @Override
    public void run() {
        listen.set(true);
        while (listen.get()) {
            try {
                String str = pReadln(null);
                if (listen.get()) {
                    for (Processor listener : listeners) {
                        listener.echo(str);
                    }
                }
            } catch (IOException ex) {
                logger.error(ex.getLocalizedMessage());
            }
        }
    }

    private String pReadln(Map<String, String> params) throws IOException {
        String result = reader.readLine();
        return result;
    }

    @Override
    public String readln(Map<String, String> params) throws IOException {
        if (listen.get()) {
            throw new RuntimeException("Can't use while listening.");
        }
        return pReadln(params);
    }

    @Override
    public void echo(Serializable object, Object... args) {
        System.out.println(object.toString());
    }

}
