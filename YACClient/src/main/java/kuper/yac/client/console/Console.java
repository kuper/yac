package kuper.yac.client.console;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import kuper.yac.api.Processor;

/**
 *
 * @author kuper
 */
public abstract class Console extends Thread implements Processor {

    public abstract String readln(Map<String, String> params) throws IOException;

    protected final AtomicBoolean listen = new AtomicBoolean(false);

    protected final Set<Processor> listeners = new HashSet<>();


    public boolean isListen() {
        return listen.get();
    }


    public void stopListen() {
        listen.set(false);
    }
    
    @Override
    public void registerOutput(Processor output) {
        listeners.add(output);
    }

    @Override
    public void unregisterOutput(Processor protocol) {
        listeners.remove(protocol);
    }

}
