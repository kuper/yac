package kuper.yac.client.console;

import java.io.IOException;
import kuper.yac.api.dto.AuthResponse;

/**
 *
 * @author vkuprin
 */
public interface AuthDialog {
     AuthResponse dialog() throws IOException;
}
