package kuper.yac.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.net.SocketFactory;
import kuper.yac.api.ApplicationContextProvider;
import kuper.yac.api.Protocol;
import kuper.yac.client.console.Console;
import kuper.yac.client.protocol.XMLNegotiationProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author vkuprin
 */
public class Manager implements Runnable {

    private final Logger logger = LoggerFactory.getLogger(Manager.class);

    private final AtomicBoolean stop = new AtomicBoolean(false);

    private Thread session;

    private Console console;

    private String address;

    private Integer port;
    
    private String userName;
    
    private String passwd;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public Thread getSession() {
        return session;
    }

    public void setSession(Thread session) {
        this.session = session;
    }

    public Console getConsole() {
        return console;
    }

    public void setConsole(Console console) {
        this.console = console;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
    
    @Override
    public void run() {
        ApplicationContext factory = ApplicationContextProvider.getContext();
        try (Socket socket = SocketFactory.getDefault().createSocket(address,
                port);
             InputStream input = socket.getInputStream();
             OutputStream output = socket.getOutputStream();) {

            Protocol protocol = (Protocol) factory.getBean("negotiationProtocol");
            protocol.setInput(input);
            protocol.setOutput(output);
            
            ((XMLNegotiationProtocol) protocol).setManager(this);
            
            logger.trace("Before process");
            protocol.process();
            logger.trace("After process");
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
        }
        if (null != session && session.isAlive()) {
            try {
                console.setName("console"+console.getName());
                console.start();
                session.join();
                console.stopListen();
            } catch (InterruptedException ex) {
                logger.trace("Interrupted");
            }
        }
    }

}
