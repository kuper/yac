package kuper.yac.client;

import java.io.IOException;
import kuper.yac.client.console.ConsoleImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author vkuprin
 */
public class Application {

    static private final Logger logger = LoggerFactory.getLogger(
            Application.class);

    private static String address = "";
    private static Integer port = null;
//    private static String userName = "";
//    private static char[] passwd;

    public static void main(String[] args) {
        
        ApplicationContext context
                = new ClassPathXmlApplicationContext("application-context.xml");
        BeanFactory factory = (BeanFactory) context;

        ConsoleImpl console = (ConsoleImpl) factory.getBean("console");
        
        for (String arg : args) {
            if (arg.startsWith("-a")) {
                address = arg.substring(2);
            } else if (arg.startsWith("-p")) {
                try {
                    port = Integer.parseInt(arg.substring(2));
                } catch (NumberFormatException ex) {
                    logger.error("Wrong port argument");
                }
            }
        }
        
        if (address.isEmpty()) {
            console.echo("Enter server address", null);
            while (address.isEmpty()) {
                try {
                    while ((address = console.readln(null)).isEmpty()) {

                    }
                } catch (IOException ex) {
                    logger.error("Smth very strange happened");
                }
            }
        }
        
        Manager manager = (Manager) factory.getBean("manager");
        manager.setAddress(address);
        if (null != port) {
            manager.setPort(port);
        } else if (null != manager.getPort()) {
            console.echo(String.format("Using default port. %s", manager.getPort()));
            logger.info("Using default port. {}", manager.getPort());
        } else {
            console.echo("Port not set. Exiting.");
            logger.error("Port not set. Exiting.");
        }
        manager.run();
    }

}
