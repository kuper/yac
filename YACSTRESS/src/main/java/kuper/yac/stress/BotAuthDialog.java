package kuper.yac.stress;

import java.io.IOException;
import kuper.yac.api.dto.AuthResponse;
import kuper.yac.client.console.AuthDialog;

/**
 *
 * @author vkuprin
 */
public class BotAuthDialog implements AuthDialog{

    private static int count = 0;
    
    private final static String NAME_TEMPLATE = "Bot %d";
    
    @Override
    synchronized public AuthResponse dialog() throws IOException {
        
        AuthResponse res = new AuthResponse();
        res.setName(String.format(NAME_TEMPLATE, ++count));
        res.setPasswd("1");
        return res;
    }
    
}
