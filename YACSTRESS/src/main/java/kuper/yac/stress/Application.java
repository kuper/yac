package kuper.yac.stress;

import java.util.logging.Level;
import kuper.yac.client.Manager;
import kuper.yac.client.console.Console;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author kuper
 */
public class Application {

    private static Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {

        ApplicationContext context
                = new ClassPathXmlApplicationContext("application-context.xml");
        BeanFactory factory = (BeanFactory) context;

        String address = "127.0.0.1";
        Integer port = 10000;
        int clientsCount = 1000;
        for (String arg : args) {
            if (arg.startsWith("-a")) {
                address = arg.substring(2);
            } else if (arg.startsWith("-p")) {
                try {
                    port = Integer.parseInt(arg.substring(2));
                } catch (NumberFormatException ex) {
                    logger.error("Wrong port argument");
                }
            } else if (arg.startsWith("-c")) {
                try {
                    clientsCount = Integer.parseInt(arg.substring(2));
                } catch (NumberFormatException ex) {
                    logger.error("-c is not a number");
                }
            }
        }

        
        for (int i = 0; i<clientsCount; i++) {
            Console console = (Console) factory.getBean("console");
            Manager manager = (Manager) factory.getBean("manager");
            manager.setAddress(address);
            manager.setPort(port);
            manager.setConsole(console);
            Thread t = new Thread(manager);
            t.start();
            System.out.println("Started bot " + " " +i);
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                java.util.logging.Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
