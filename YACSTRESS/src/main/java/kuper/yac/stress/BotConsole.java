package kuper.yac.stress;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import kuper.yac.api.Processor;
import kuper.yac.client.console.Console;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vkuprin
 */
public class BotConsole extends Console {

    String[] phrases = {"Привет!", "Как дела?", "ОК", "Пока!", "Абракадабра"};

    private Logger logger = LoggerFactory.getLogger(BotConsole.class);

    @Override
    public void run() {
        listen.set(true);
        try {
            while (listen.get()) {
                if (Math.round(Math.random() * 60) > 50) {
                    int phraseNum = (int) Math.round((phrases.length - 1) * Math.random());
                    String str = phrases[phraseNum];
                    if (listen.get()) {
                        for (Processor listener : listeners) {
                            listener.echo(str);
                        }
                    }
                }
                Long waitFor = Math.round(120000 + Math.random() * 600000);
                sleep(waitFor);
            }
        } catch (InterruptedException ex) {
            logger.error(ex.getLocalizedMessage());
        }
    }

    @Override
    public String readln(Map<String, String> params) throws IOException {
        return " ";
    }

    @Override
    public void echo(Serializable object, Object... args) {
        logger.info("got message");
    }

}
