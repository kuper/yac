package kuper.yac.server.protocol;

import java.io.Serializable;
import java.util.List;
import kuper.yac.api.CommandExecutor;
import kuper.yac.api.Processor;
import kuper.yac.api.dto.Command;
import kuper.yac.api.dto.Message;
import kuper.yac.server.database.DBManager;

/**
 *
 * @author kuper
 */
public class HistoryCommand implements CommandExecutor{

    private DBManager dbManager;

    public DBManager getDbManager() {
        return dbManager;
    }

    public void setDbManager(DBManager dbManager) {
        this.dbManager = dbManager;
    }
    
    @Override
    public Object execute(Command command, Object... args) {
        List<Serializable> res = dbManager.getAll();
        Processor proc = (Processor) args[0];
        for (Serializable re : res) {
            proc.echo(re, args);
        }
        return null;
    }
    
    
    
}
