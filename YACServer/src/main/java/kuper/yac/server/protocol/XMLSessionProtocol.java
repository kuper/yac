package kuper.yac.server.protocol;

import kuper.yac.api.XMLProtocol;
import java.io.Serializable;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import kuper.yac.api.ApplicationContextProvider;
import kuper.yac.api.Processor;
import kuper.yac.api.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author vkuprin
 */
public class XMLSessionProtocol extends XMLProtocol implements Processor {

    private final Logger logger = LoggerFactory.getLogger(
            XMLSessionProtocol.class);

    private Authenticator authenticator;

    public Authenticator getAuthenticator() {
        return authenticator;
    }

    private List<String> outputBeans;
    private List<String> inputBeans;

    public List<String> getInputBeans() {
        return inputBeans;
    }

    public void setInputBeans(List<String> inputBeans) {
        this.inputBeans = inputBeans;
    }

    public List<String> getOutputBeans() {
        return outputBeans;
    }

    public void setOutputBeans(List<String> outputBeans) {
        this.outputBeans = outputBeans;
    }

    public void setAuthenticator(Authenticator authenticator) {
        this.authenticator = authenticator;
    }

    @Override
    public boolean isStarted() {
        return !state.equals(States.GREETING);
    }

    @Override
    public void stop() {
        listen = false;
        state = States.STOPPED;
    }

    private enum States {
        GREETING, WAIT_AUTH, CHATING, STOPPED
    }

    private States state = States.GREETING;
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setAuthenticator(SimpleAuthenticatorImpl authenticator) {
        this.authenticator = authenticator;
    }

    @Override
    public void process() {
        try {
            try {
                createWriter();
                synchronized (writer) {
                    writer.writeStartElement("root");
                    encoder.encode(writer, new AuthRequest());
                    state = States.WAIT_AUTH;
                    createReader();
                }
                listen();
                synchronized (writer) {
                    writer.writeEndElement();
                }
            } catch (XMLStreamException ex) {
                logger.error("XML error");
            }
        } finally {
            unregisterSession();
        }
    }

    @Override
    public void echo(Serializable object, Object... args) {
        if (state.equals(States.CHATING) && listen) {
            encoder.encode(writer, object);
        }
    }

    private void auth(AuthResponse response) {
        userName = response.getName();
        String passwd = response.getPasswd();
        Confirmation confirmation = new Confirmation();
        if (authenticator.authUser(userName, passwd)) {
            if (authenticator.hasActiveSession(userName)) {
                confirmation.setState(Confirmation.State.REJECT);
                confirmation.setInfo("User already logged in");
                state = States.STOPPED;
            } else {
                registerSession();
                confirmation.setState(Confirmation.State.SUCCESS);
                state = States.CHATING;
            }
        } else {
            confirmation.setState(Confirmation.State.FAILURE);
            confirmation.setInfo("Wrong password or unknown user");
            state = States.STOPPED;
        }
        encoder.encode(writer, confirmation);
    }

    private void registerSession() {
        logger.debug("Registering session for user {}", userName);
        authenticator.setUserSession(userName, this);
        ApplicationContext ctx = ApplicationContextProvider.getContext();
        for (String outputBean : outputBeans) {
            this.registerOutput((Processor) ctx.getBean(outputBean));
        }
        for (String inputBean : inputBeans) {
            ((Processor) ctx.getBean(inputBean)).registerOutput(this);
        }
    }

    private void unregisterSession() {
        logger.debug("Unegistering session for user {}", userName);
        authenticator.removeUserSession(userName);
        ApplicationContext ctx = ApplicationContextProvider.getContext();
        for (String inputBean : inputBeans) {
            ((Processor) ctx.getBean(inputBean)).unregisterOutput(this);
        }
    }

    @Override
    protected void processDTO(Serializable dto) {
        //logger.debug("processing dto:" + dto.toString());
        if (state.equals(States.WAIT_AUTH) && (dto instanceof AuthResponse)) {
            AuthResponse response = (AuthResponse) dto;
            auth(response);
        } else if (state.equals(States.CHATING)) {
            if (dto instanceof Message) {
                if (((Message) dto).getAuthor().equals(userName)) {
                    super.echo(dto);
                } else {
                    logger.error("Session user and message userName not same!");
                }
            } else if (dto instanceof Command) {
                if (((Command) dto).getAuthor().equals(userName)) {
                    super.echo(dto, this);
                } else {
                    logger.error("Session user and command userName not same!");
                }
            }
        } else {
            logger.error("Got unsupported DTO: " + dto.toString());
        }
        if (state.equals(States.STOPPED)) {
            listen = false;
        }
    }

}
