package kuper.yac.server.protocol;

import kuper.yac.api.XMLProtocol;
import java.io.Serializable;
import javax.xml.stream.XMLStreamException;
import kuper.yac.api.dto.*;
import kuper.yac.api.utils.*;
import kuper.yac.server.transport.SessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vkuprin
 */
public class XMLNegotiationProtocol extends XMLProtocol {

    private Integer port = null;

    private boolean started = false;

    private final Logger logger = LoggerFactory.getLogger(
            XMLNegotiationProtocol.class);

    private SessionManager sessionManager;
    private Authenticator authenticator;

    @Override
    public boolean isStarted() {
        return started;
    }

    @Override
    public void stop() {
        try {
            state = States.STOPPED;
            listen = false;
            reader.close();
        } catch (XMLStreamException ex) {
            logger.error(ex.getMessage());
        }
    }

    private enum States {
        GREETING, WAIT_AUTH, WAIT_PORT_CONFIRM, STOPPED
    }

    private States state = States.GREETING;

    private States getState() {
        return state;
    }

    private void setState(States state) {
        logger.trace("setting {} state {}",Thread.currentThread().getName() ,state);
        this.state = state;
    }
    
    private final MyXmlPojoEncoder encoder = new MyXmlPojoEncoder();

    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public void setAuthenticator(SimpleAuthenticatorImpl authenticator) {
        this.authenticator = authenticator;
    }

    @Override
    public void process() {
        try {
            createWriter();

            writer.writeStartElement("root");
            started = true;
            encoder.encode(writer, new AuthRequest());
            setState(States.WAIT_AUTH);

            createReader();
            listen();

            writer.writeEndElement();

        } catch (XMLStreamException ex) {
            logger.error("XML error");
        }
    }

    private void auth(AuthResponse response) {
        String userName = response.getName();
        String passwd = response.getPasswd();
        Confirmation confirmation = new Confirmation();
        if (authenticator.authUser(userName, passwd)) {
            if (authenticator.hasActiveSession(userName)) {
                confirmation.setState(Confirmation.State.REJECT);
                confirmation.setInfo("User already logged in");
                setState(States.GREETING);
            } else {
                port = sessionManager.getPort();
                confirmation.setState(Confirmation.State.SUCCESS);
                confirmation.setInfo(port.toString());
                setState(States.WAIT_PORT_CONFIRM);
            }
        } else {
            confirmation.setState(Confirmation.State.FAILURE);
            confirmation.setInfo("Wrong password or unknown user");
            setState(States.GREETING);
        }
        encoder.encode(writer, confirmation);
        if (state == States.GREETING) {
            encoder.encode(writer, new AuthRequest());
            setState(States.WAIT_AUTH);
        }
    }

    @Override
    protected void processDTO(Serializable dto) {
        if (state.equals(States.WAIT_AUTH) && (dto instanceof AuthResponse)) {
            AuthResponse response = (AuthResponse) dto;
            auth(response);
        } else if (state.equals(States.WAIT_PORT_CONFIRM) && (dto instanceof Confirmation)) {
            Confirmation confirmation = (Confirmation) dto;
            if (confirmation.getState().equals(Confirmation.State.CONFIRM)) {
                setState(States.STOPPED);
                listen = false;
            } else {
                setState(States.STOPPED);
                listen = false;
            }
        } else {
            logger.error("Wrong DTO: {}", dto.toString());
        }
    }

}
