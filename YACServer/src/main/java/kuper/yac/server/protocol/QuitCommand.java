package kuper.yac.server.protocol;

import kuper.yac.api.CommandExecutor;
import kuper.yac.api.Processor;
import kuper.yac.api.Protocol;
import kuper.yac.api.dto.Command;
import kuper.yac.api.dto.Confirmation;

/**
 *
 * @author kuper
 */
public class QuitCommand implements CommandExecutor {

    @Override
    public Object execute(Command command, Object... params) {
        Processor proc = (Processor) params[0];
        Confirmation result = new Confirmation();
        result.setState(Confirmation.State.CONFIRM);
        result.setInfo("Goodbye!");
        proc.echo(result);
        Protocol session = (Protocol) params[0];
        session.stop();
        return result; 
    }
    
}
