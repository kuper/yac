package kuper.yac.server.protocol;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import kuper.yac.api.ApplicationContextProvider;
import kuper.yac.api.CommandExecutor;
import kuper.yac.api.Processor;
import kuper.yac.api.SimpleProcessor;
import kuper.yac.api.dto.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * @author kuper
 */
public class CommandProcessor extends SimpleProcessor {

    private final Logger logger = LoggerFactory.getLogger(CommandProcessor.class);

    private Map<String, String> commands = new HashMap<>();
    private final ApplicationContext ctx = ApplicationContextProvider.getContext();

    public Map<String, String> getCommands() {
        return commands;
    }

    public void setCommands(Map<String, String> commands) {
        this.commands = commands;
    }

    @Override
    public void echo(Serializable object, Object... args) {
        if (object instanceof Command) {
            Command command = (Command) object;
            String executorName = commands.get(command.getCommand());
            if (null != executorName) {
                try {
                    CommandExecutor executor = (CommandExecutor) ctx.getBean(
                            executorName);
                    executor.execute(command, args);
                } catch (BeansException ex) {
                    logger.error("Command not found: {}", command.getCommand());
                }
            }

        }
    }

}
