package kuper.yac.server.protocol;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import kuper.yac.api.Protocol;

/**
 *
 * @author kuper
 */
public class SimpleAuthenticatorImpl implements Authenticator {

    private final Map<String, Protocol> userSessions = Collections.synchronizedMap(new HashMap<>());

    @Override
    public Boolean authUser(String name, String authString) {
        return (name != null && !name.isEmpty());
    }

    @Override
    public boolean hasActiveSession(String userName) {
        synchronized (userSessions) {
            if (userSessions.containsKey(userName)) {
                Protocol session = userSessions.get(userName);
                if (null != session) {
                    return session.isActive();
                }
            }
            return false;
        }
    }

    @Override
    public boolean setUserSession(String userName, Protocol session) {
        synchronized (userSessions) {
            if (!hasActiveSession(userName)) {
                userSessions.put(userName, session);
                return true;
            }
            return false;
        }
    }

    @Override
    public void removeUserSession(String userName) {
        synchronized (userSessions) {
            userSessions.remove(userName);
        }
    }

}
