package kuper.yac.server.protocol;

import kuper.yac.api.Protocol;

/**
 *
 * @author kuper
 */
public interface Authenticator {

    /**
     *
     * @param name userName
     * @param authString password or hash
     * @return 
     */
    Boolean authUser(String name, String authString);

    public boolean hasActiveSession(String userName);
    
    public boolean setUserSession(String userName, Protocol session); 
    
    public void removeUserSession(String userName); 
    
}
