package kuper.yac.server.transport;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import kuper.yac.api.ApplicationContextProvider;
import kuper.yac.api.Protocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kuper
 */
public class TransportThread extends Thread {

    private Socket socket = null;

    private String protocolBeanName;

    private YacThreadListener parentThread;

    private final Logger logger = LoggerFactory.getLogger(TransportThread.class);

    public YacThreadListener getParentThread() {
        return parentThread;
    }

    public void setParentThread(YacThreadListener parentThread) {
        this.parentThread = parentThread;
    }

    public void setProtocolBeanName(String protocolBeanName) {
        this.protocolBeanName = protocolBeanName;
        long stamp = System.currentTimeMillis();
        this.setName(String.format((protocolBeanName + "Thread-%s"), stamp));
    }

    public TransportThread() {
        super();
    }

    @Override
    public void run() {
        try {
            parentThread.onChildStarted(this);
            try (OutputStream out = socket.getOutputStream();
                 InputStream in = socket.getInputStream();) {
                Protocol protocol = ApplicationContextProvider.getContext().getBean(
                        protocolBeanName, Protocol.class);
                protocol.setInput(in);
                protocol.setOutput(out);
                protocol.process();
            } catch (IOException e) {
                logger.error("Negotiation process failed", e);
            }
            try {
                if (!socket.isClosed()) {
                    logger.debug("socket closing");
                    socket.close();
                    
                }
            } catch (IOException ex) {
                logger.error("TransportThread IO exception");
            }
        } finally {
            parentThread.onChildEnded(this);
        }
    }

    void setSocket(Socket socket) {
        this.socket = socket;
    }

}
