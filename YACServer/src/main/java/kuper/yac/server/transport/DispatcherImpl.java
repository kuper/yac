/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kuper.yac.server.transport;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import kuper.yac.api.ApplicationContextProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;

/**
 *
 * @author kuper
 */
public class DispatcherImpl implements Dispatcher, YacThreadListener {

    private final AtomicBoolean shutdown = new AtomicBoolean(false);

    private final Logger logger = LoggerFactory.getLogger(DispatcherImpl.class);

    private Integer port = 0;

    private ServerSocket serverSocket = null;
    
    private int threadCount = 0;
    
    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        if (null != serverSocket && !serverSocket.isClosed()) {
            throw new RuntimeException("Server already running. Can't change port");
        }
        this.port = port;
    }

    public DispatcherImpl() {
        super();
    }

    public DispatcherImpl(Integer port) {
        this.port = port;
    }

    private BeanFactory factory;
    
    @Override
    public void run() {
        factory = ApplicationContextProvider.getContext();
        try (ServerSocket serverSocket = new ServerSocket(port, 1000);) {
            this.serverSocket = serverSocket;
            serverSocket.setSoTimeout(java.net.SocketOptions.SO_TIMEOUT);
            serverSocket.setReuseAddress(true);
            logger.info("Listen on port {}", serverSocket.getLocalPort());
            System.out.println(String.format("Listen on port %d", serverSocket.getLocalPort()));
            listenNego(serverSocket);
        } catch (IllegalArgumentException ex) {
            logger.error("Failed to open socket: illegal port number {}!", port);
        } catch (SecurityException ex) {
            logger.error("Failed to open socket: {} port usage forbidden!", port);
        } catch (IOException ex) {
            logger.error("General I/O exeception!", ex);
        }

    }

    @Override
    public void shutdown() {
        logger.trace("Shuting down!");
        shutdown.set(true);
    }

    private void listenNego(ServerSocket serverSocket) {
        while (!shutdown.get()) {
            try {
                Socket socket = serverSocket.accept();
                TransportThread tt = factory.getBean("negotiationThread", TransportThread.class);
                tt.setSocket(socket);
                tt.setParentThread(this);
                tt.start();
            } catch (SocketTimeoutException ex) {
                logger.trace("Socket timeout. It's OK");
            } catch (IOException ex) {
                logger.error("Can't create connection");
            } 
        }
    }

    public int getThreadCount() {
        return threadCount;
    }

    @Override
    synchronized public void onChildStarted(Thread child) {
        threadCount++;
    }

    @Override
    synchronized public void onChildEnded(Thread child) {
        threadCount--;
    }

}
