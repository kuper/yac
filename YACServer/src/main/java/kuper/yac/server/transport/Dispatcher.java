package kuper.yac.server.transport;

/**
 *
 * @author kuper
 */
public interface Dispatcher {

   void run(); 
   void shutdown();
    
}
