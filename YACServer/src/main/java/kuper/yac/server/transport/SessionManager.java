package kuper.yac.server.transport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kuper
 */
public class SessionManager {

    private final Logger logger = LoggerFactory.getLogger(SessionManager.class);

    private final List<SessionBucket> transportSocketsPool = Collections.synchronizedList(
            new ArrayList<>());

    private int maxConnectionsOnPort = 10;

    public int getMaxConnectionsOnPort() {
        return maxConnectionsOnPort;
    }

    public void setMaxConnectionsOnPort(int maxConnectionsOnPort) {
        this.maxConnectionsOnPort = maxConnectionsOnPort;
    }

    public int getPort() {
        synchronized (transportSocketsPool) {
            if (transportSocketsPool.isEmpty()) {
                logger.debug("Socket pool empty. Create new port.");
                return addBucket();
            } else {
                for (SessionBucket sessionBucket : transportSocketsPool) {
                    if (sessionBucket.getThreadCount() < maxConnectionsOnPort) {
                        int port = sessionBucket.getPort();
                        logger.debug("return port {}", port);
                        return sessionBucket.getPort();
                    }
                }
                return addBucket();
            }
        }
    }

    synchronized private int addBucket() {
        SessionBucket bucket = new SessionBucket();
        int port = 0;
        bucket.start();
        while (port == 0 && bucket.isAvailable()) {
            port = bucket.getPort();
        }
        if (port != 0) {
            transportSocketsPool.add(bucket);
        }
        logger.debug("{} Bucket added", transportSocketsPool.size());
        return port;
    }
}
