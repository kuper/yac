package kuper.yac.server.transport;

/**
 *
 * @author kuper
 */
public interface YacThreadListener {
    void onChildStarted(Thread child);
    void onChildEnded(Thread child);
}
