package kuper.yac.server.transport;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import kuper.yac.api.ApplicationContextProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;

/**
 *
 * @author kuper
 */
public class SessionBucket extends Thread implements YacThreadListener {

    private final Logger logger = LoggerFactory.getLogger(SessionBucket.class);

    private final AtomicBoolean shutdown = new AtomicBoolean(false);

    private ServerSocket serverSocket;
    private AtomicInteger port = new AtomicInteger(0);
    private AtomicBoolean init = new AtomicBoolean(true);

    private Integer threadCount = 0;

    private BeanFactory factory;

    public Integer getThreadCount() {
        return threadCount;
    }

    private void listen() throws IOException {
        while (!serverSocket.isClosed() && !shutdown.get()) {
            try {
                Socket socket = serverSocket.accept();
                TransportThread tt = factory.getBean("sessionThread",
                        TransportThread.class);
                tt.setSocket(socket);
                tt.setParentThread(this);
                tt.start();
            } catch (SocketTimeoutException ex) {
                logger.trace("Socket timeout. It's OK");
            } catch (IOException ex) {
                logger.error("Can't create connection");
            }
        }

    }

    public int getPort() {
        return port.get();
    }

    public boolean isAvailable() {
        return init.get() || ((null != serverSocket) && !serverSocket.isClosed() && serverSocket.isBound());
    }

    @Override
    public void run() {
        logger.debug("Try to create new bucket");
        this.setName("Bucket" + this.getName());;
        factory = ApplicationContextProvider.getContext();
        try (ServerSocket serverSocket = new ServerSocket(0, 50);) {
            this.serverSocket = serverSocket;
            serverSocket.setSoTimeout(java.net.SocketOptions.SO_TIMEOUT);
            serverSocket.setReuseAddress(true);
            port.set(serverSocket.getLocalPort());
            logger.info("New transport listen  on port {}", port);
            init.set(false);
            listen();
        } catch (IllegalArgumentException ex) {
            logger.error("Failed to open socket: illegal port number {}!", port);
        } catch (SecurityException ex) {
            logger.error("Failed to open socket: {} port usage forbidden!", port);
        } catch (IOException ex) {
            logger.error("General I/O exeception!", ex);
        } finally {
            logger.debug("Fall to finally");
            init.set(false);
        }
    }

    @Override
    synchronized public void onChildStarted(Thread child) {
        threadCount++;
    }

    @Override
    synchronized public void onChildEnded(Thread child) {
        threadCount--;
    }

}
