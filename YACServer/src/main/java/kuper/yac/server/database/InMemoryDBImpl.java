package kuper.yac.server.database;

import kuper.yac.api.Callback;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import kuper.yac.api.Processor;
import kuper.yac.api.dto.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kuper
 */
public class InMemoryDBImpl implements DBManager, Processor {

    private final Logger logger = LoggerFactory.getLogger(InMemoryDBImpl.class);

    private int maxSize;

    private final List<Message> storage = Collections.synchronizedList(new LinkedList<>());

    private final Set<Processor> listeners = Collections.synchronizedSet(
            new HashSet<>());

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public InMemoryDBImpl() {
        super();
    }

    @Override
    public void registerOutput(Processor listener) {
        synchronized (listeners) {
            listeners.add(listener);
        }
        logger.debug("New listener subscribed. Listeners count {}", listeners.size());
    }

    @Override
    public void unregisterOutput(Processor listener) {
        synchronized (listeners) {
            listeners.remove(listener);
        }
        logger.debug("Listener unsubscribed. Listeners count {}", listeners.size());
    }

    @Override
    public void echo(Serializable object, Object... args) {
        if (object instanceof Message) {
            Message message = (Message) object;
            synchronized (storage) {
                if (storage.size() == this.maxSize) {
                    storage.remove(0);
                } else if (storage.size() > this.maxSize) {
                    logger.error("It's must not happen!!!");
                    while (storage.size() >= this.maxSize) {
                        storage.remove(0);
                    }
                }
                if (storage.add(message)) {
                    notify(message);
                }
            }
        }
    }

    private void notify(Serializable object) {
        synchronized (listeners) {
            long start = System.nanoTime();
            logger.debug("send message to {} listeners", listeners.size());
            for (Processor listener : listeners) {
                listener.echo(object);
            }
            long time = System.nanoTime() - start;
            logger.debug("send message finished, {} long", time);
        }
    }

    @Override
    public List getAll() {
        synchronized (storage) {
            return Collections.unmodifiableList(new ArrayList<>(storage));
        }
    }

}
