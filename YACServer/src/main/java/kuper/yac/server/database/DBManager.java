package kuper.yac.server.database;

import kuper.yac.api.Callback;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author kuper
 * @param <T>
 */
public interface DBManager {

    List<Serializable> getAll();
    
}
