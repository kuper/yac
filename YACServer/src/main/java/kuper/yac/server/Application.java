package kuper.yac.server;

import kuper.yac.server.transport.Dispatcher;
import kuper.yac.server.transport.DispatcherImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author kuper
 */
public class Application {

    static private DispatcherImpl dispatcher = null;
    static private final Logger logger = LoggerFactory.getLogger(
            Application.class);

    public static void main(String[] args) {
        ApplicationContext context
                = new ClassPathXmlApplicationContext("application-context.xml");
        BeanFactory factory = (BeanFactory) context;
        registerShutdownHook();

        Integer port = null;
        for (String arg : args) {
            if (arg.startsWith("-p")) {
                try {
                    port = Integer.parseInt(arg.substring(2));
                } catch (NumberFormatException ex) {
                    logger.error("Wrong port argument");
                }
            }
        }

        dispatcher = factory.getBean("dispatcher", DispatcherImpl.class);
        if (null != port) {
            dispatcher.setPort(port);
        }
        dispatcher.run();
    }

    private static void registerShutdownHook() {
        final Thread mainThread = Thread.currentThread();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    logger.debug("Got terminate signal!");
                    Application.dispatcher.shutdown();
                    mainThread.join(0);
                } catch (InterruptedException ex) {
                    logger.error("Interrupted");
                }
            }
        });
    }

}
