package kuper.yac.yacserver.database;

import java.io.Serializable;
import kuper.yac.server.database.InMemoryDBImpl;
import kuper.yac.api.Callback;
import kuper.yac.api.dto.Message;
import java.util.List;
import kuper.yac.api.Processor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kuper
 */
public class InMemoryDBImplTest {

    Logger logger = LoggerFactory.getLogger(InMemoryDBImplTest.class);

    public InMemoryDBImplTest() {
    }

    InMemoryDBImpl instance;
    Listener l;
    
    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of put method, of class InMemoryDBImpl.
     */
    @Test
    public void testPutAndGetAll() {
        System.out.println("put");
        InMemoryDBImpl instance = new InMemoryDBImpl();
        instance.setMaxSize(5);
        Listener l = new Listener(1);
        instance.registerOutput(l);
        l = new Listener(2);
        instance.registerOutput(l);
        
        boolean expResult;
        boolean result;
        for (int i = 1; i <= 10; i++) {
            Message object = new Message(String.valueOf(i),
                    "test message" + String.valueOf(i));
            expResult = true;
            instance.echo(object);
            List<Message> answer = instance.getAll();
            assertEquals((i<5)?i:5, answer.size());
            assertEquals(object, answer.get(answer.size()-1));
        }

    }

    /**
     * Test of getAll method, of class InMemoryDBImpl.
     */
//    @Test
//    public void testGetAll() {
////        System.out.println("getAll");
////        InMemoryDBImpl instance = null;
////        List expResult = null;
////        List result = instance.getAll();
////        assertEquals(expResult, result);
////        // TODO review the generated test code and remove the default call to fail.
////        fail("The test case is a prototype.");
//    }

    class Listener implements Processor {

        long id;

        public Listener(long id) {
            this.id = id;
        }

        @Override
        public void echo(Serializable object, Object... args) {
            Message message = (Message) object;
            logger.info(
                    "Listener {} got message  author: {}\n time: {}\n message: {}",
                    id, message.getAuthor(),
                    message.timestampString(), message.getMessage());
        }

        @Override
        public void registerOutput(Processor output) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void unregisterOutput(Processor output) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

}
