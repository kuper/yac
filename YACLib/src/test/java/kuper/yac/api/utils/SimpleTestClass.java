package kuper.yac.api.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author kuper
 */
 public class SimpleTestClass implements Serializable {

        private Integer intField;
        private Integer integerField;
        private String stringField;
        private Long longField;
        private Date dateField;
        private Boolean booleanField;

        public Integer getIntField() {
            return intField;
        }

        public void setIntField(Integer intField) {
            this.intField = intField;
        }

        public Integer getIntegerField() {
            return integerField;
        }

        public void setIntegerField(Integer integerField) {
            this.integerField = integerField;
        }

        public String getStringField() {
            return stringField;
        }

        public void setStringField(String stringField) {
            this.stringField = stringField;
        }

        public Long getLongField() {
            return longField;
        }

        public void setLongField(Long longField) {
            this.longField = longField;
        }

        public Date getDateField() {
            return dateField;
        }

        public void setDateField(Date dateField) {
            this.dateField = dateField;
        }

        public Boolean getBooleanField() {
            return booleanField;
        }

        public void setBooleanField(Boolean booleanField) {
            this.booleanField = booleanField;
        }

        public SimpleTestClass() {
        }

        public SimpleTestClass(Integer intField, Integer integerField,
                String stringField, Long longField, Date dateField,
                Boolean booleanField) {
            this.intField = intField;
            this.integerField = integerField;
            this.stringField = stringField;
            this.longField = longField;
            this.dateField = dateField;
            this.booleanField = booleanField;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 67 * hash + this.intField;
            hash = 67 * hash + Objects.hashCode(this.integerField);
            hash = 67 * hash + Objects.hashCode(this.stringField);
            hash = 67 * hash + Objects.hashCode(this.longField);
            hash = 67 * hash + Objects.hashCode(this.dateField);
            hash = 67 * hash + Objects.hashCode(this.booleanField);
            return hash;
        }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SimpleTestClass other = (SimpleTestClass) obj;
        if (!Objects.equals(this.stringField, other.stringField)) {
            return false;
        }
        if (!Objects.equals(this.intField, other.intField)) {
            return false;
        }
        if (!Objects.equals(this.integerField, other.integerField)) {
            return false;
        }
        if (!Objects.equals(this.longField, other.longField)) {
            return false;
        }
        if (!Objects.equals(this.dateField, other.dateField)) {
            return false;
        }
        if (!Objects.equals(this.booleanField, other.booleanField)) {
            return false;
        }
        return true;
    }

        
    }
