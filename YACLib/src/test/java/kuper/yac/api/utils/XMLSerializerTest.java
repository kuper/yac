package kuper.yac.api.utils;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Date;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kuper
 */
public class XMLSerializerTest {

    public XMLSerializerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void serialize_deserialize() {
        try {
            System.out.println("toXML");
            OutputStream out = new ByteArrayOutputStream();
            
            XMLOutputFactory factory = XMLOutputFactory.newInstance();
            XMLStreamWriter writer = factory.createXMLStreamWriter(out);
            SimpleTestClass field1 = new SimpleTestClass(78, new Integer(150),
                    "stringField", new Long(0xFDFFA), new Date(), Boolean.FALSE);
            SimpleTestClass field2 = new SimpleTestClass(78 * 3,
                    new Integer(5 * 150),
                    "stringField", new Long(0xFDFFA - 150), new Date(79456L),
                    Boolean.TRUE);
            ComplexTestClass instance = new ComplexTestClass();
            instance.setField1(field1);
            instance.setField2(field2);
            
            boolean expResult = true;
            new MyXmlPojoEncoder().encode(writer, instance);
            String res = out.toString();
            System.out.println("Вывод:");
            System.out.println(res);
            System.out.println("from XML:");
            
            XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(
                    new ByteArrayInputStream(res.getBytes()));
            
            reader.nextTag();
            Object copy = (new MyXmlPojoDecoder(reader).decode());
            System.out.println(copy.hashCode());
            assertTrue(copy.equals(instance));
            assertTrue(true);
        } catch (XMLStreamException ex) {
            fail();
        }
    }

}
