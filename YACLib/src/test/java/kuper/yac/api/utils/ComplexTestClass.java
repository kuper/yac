package kuper.yac.api.utils;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author kuper
 */
public class ComplexTestClass implements Serializable {

        private SimpleTestClass field1;
        private SimpleTestClass field2;
        private Date field3;

        public Date getField3() {
            return field3;
        }

        public void setField3(Date field3) {
            this.field3 = field3;
        }

        public SimpleTestClass getField1() {
            return field1;
        }

        public void setField1(SimpleTestClass field1) {
            this.field1 = field1;
        }

        public SimpleTestClass getField2() {
            return field2;
        }

        public void setField2(SimpleTestClass field2) {
            this.field2 = field2;
        }

        public ComplexTestClass(SimpleTestClass field1, SimpleTestClass field2,
                Date field3) {
            this.field1 = field1;
            this.field2 = field2;
            this.field3 = field3;
        }

        public ComplexTestClass() {
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 83 * hash + Objects.hashCode(this.field1);
            hash = 83 * hash + Objects.hashCode(this.field2);
            hash = 83 * hash + Objects.hashCode(this.field3);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ComplexTestClass other = (ComplexTestClass) obj;
            if (!Objects.equals(this.field1, other.field1)) {
                return false;
            }
            if (!Objects.equals(this.field2, other.field2)) {
                return false;
            }
            if (!Objects.equals(this.field3, other.field3)) {
                return false;
            }
            return true;
        }

    }