package kuper.yac.api;

import java.io.Serializable;

/**
 *
 * @author kuper
 */
public interface Processor {
 
    void registerOutput(Processor output);
    void unregisterOutput(Processor output);
    
    void echo(Serializable object, Object... args);
    
}
