package kuper.yac.api;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

/**
 *
 * @author kuper
 */
public interface Protocol {
    boolean isActive();
    void stop();
    boolean isStarted();
    void setInput(InputStream input);
    void setOutput(OutputStream output);
    void process();
}
