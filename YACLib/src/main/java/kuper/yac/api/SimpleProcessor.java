package kuper.yac.api;

import kuper.yac.api.Processor;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author kuper
 */
public class SimpleProcessor implements Processor {

    private Set<Processor> outputs = new HashSet<>();
    
    
    @Override
    public void registerOutput(Processor output) {
        outputs.add(output);
    }

    @Override
    public void unregisterOutput(Processor output) {
        outputs.remove(output);
    }

    @Override
    public void echo(Serializable object, Object... args) {
        for (Processor output : outputs) {
            output.echo(object, args);
        }
    }
    
}
