package kuper.yac.api;

import kuper.yac.api.dto.Command;

/**
 *
 * @author kuper
 */
public interface CommandExecutor {
    Object execute(Command command, Object... params);
}
