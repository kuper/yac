package kuper.yac.api.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author vkuprin
 */
public class AuthRequest implements Serializable {

    private String serverName;

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AuthRequest other = (AuthRequest) obj;
        if (!Objects.equals(this.serverName, other.serverName)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + Objects.hashCode(this.serverName);
        return hash;
    }

    @Override
    public String toString() {
        return "AuthRequest{" + "serverName=" + serverName + '}';
    }

}
