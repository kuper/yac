package kuper.yac.api.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author vkuprin
 */
public class Command implements Serializable {
    
    private String author;
    private String command;
    private String arguments;

    public String getArguments() {
        return arguments;
    }

    public void setArguments(String arguments) {
        this.arguments = arguments;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.author);
        hash = 89 * hash + Objects.hashCode(this.command);
        String argumentsString = String.join(" ", this.arguments); 
        hash = 89 * hash + Objects.hashCode(argumentsString);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Command other = (Command) obj;
        if (!Objects.equals(this.author, other.author)) {
            return false;
        }
        if (!Objects.equals(this.command, other.command)) {
            return false;
        }
        String thisArgumentsString = (null == this.arguments)?"":String.join(" ", this.arguments);
        String otherArgumentsString = (null == other.arguments)?"":String.join(" ", other.arguments);
        return Objects.equals(thisArgumentsString, otherArgumentsString);
    }

    @Override
    public String toString() {
        return "Command{" + "author=" + author + ", command=" + command + ", arguments=" + arguments + '}';
    }


    public void setAuthor(String userName) {
        this.author = userName;
    }

    public String getAuthor() {
        return author;
    }

}
