package kuper.yac.api.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author kuper
 */
public class Confirmation implements Serializable {
    public static final class State {
        public static final String CONFIRM = "CONFIRM";
        public static final String REJECT = "REJECT";
        public static final String FAILURE = "FAILURE";
        public static final String SUCCESS = "SUCCESS";
    }

    private String state;

    private String info;
    
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Confirmation{" + "state=" + state + ", info=" + info + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.state);
        hash = 97 * hash + Objects.hashCode(this.info);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Confirmation other = (Confirmation) obj;
        if (!Objects.equals(this.state, other.state)) {
            return false;
        }
        if (!Objects.equals(this.info, other.info)) {
            return false;
        }
        return true;
    }
    
    
}
