package kuper.yac.api.dto;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author kuper
 */
public class Message implements Serializable {

    private Date timestamp;
    private String author;
    private String message;

    private final DateFormat df = DateFormat.getDateTimeInstance(
            DateFormat.SHORT,
            DateFormat.LONG);

    public Message() {
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Message(String author, String message) {
        this.timestamp = new Date();
        this.author = author;
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String timestampString() {
        return df.format(timestamp);
    }

    public String getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public int hashCode() {
        int hash = author.hashCode() + timestamp.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Message other = (Message) obj;
        if (!Objects.equals(this.author, other.author)) {
            return false;
        }
        if (!Objects.equals(this.timestamp, other.timestamp)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n").append(this.getAuthor()).append(" ").append(this.timestampString()).append("\n").append(
                this.getMessage());
        return sb.toString();
    }

}
