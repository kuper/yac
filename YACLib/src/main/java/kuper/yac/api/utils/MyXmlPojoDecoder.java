package kuper.yac.api.utils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vkuprin
 */
public class MyXmlPojoDecoder {

    private final Logger logger = LoggerFactory.getLogger(MyXmlPojoDecoder.class);

    private final static HashSet<Class> basicClasses = new HashSet<>(
            Arrays.asList(String.class, Boolean.class, Integer.class, Long.class,
                    Date.class));

    private final XMLStreamReader reader;

    private HashMap<String, Serializable> properties = new HashMap<>();

    private Serializable object = null;
    private Class clazz = null;

    public MyXmlPojoDecoder(XMLStreamReader reader) {
        this.reader = reader;
    }

    private String currentProp;

    private int accum = 0;

    public Serializable decode() {
        try {
            synchronized (reader){
                readProperties();
            }
        } catch (XMLStreamException ex) {
            logger.error("XML Stream error: {}", ex.getLocalizedMessage());
        }
        fill();
        return object;
    }

    private void skip() throws XMLStreamException {
        while (reader.hasNext() && accum > 0) {
            Integer eventType = reader.next();
            switch (eventType) {
                case XMLStreamConstants.START_ELEMENT: {
                    accum++;
                    break;
                }
                case XMLStreamConstants.END_ELEMENT: {
                    accum--;
                    break;
                }
            }
        }
    }

    private void readProperties() throws XMLStreamException {

        String className = reader.getAttributeValue("", "class");
        if (null != className) {
            accum++;
            try {
                clazz = Class.forName(className);
            } catch (ClassNotFoundException ex) {
                logger.error("Class from xml not found");
                skip();
            }
            while (reader.hasNext() && accum > 0) {
                Integer eventType = reader.next();
                switch (eventType) {
                    case XMLStreamConstants.START_ELEMENT: {
                        startElement();
                        break;
                    }
                    case XMLStreamConstants.END_ELEMENT: {
                        endElement();
                        break;
                    }
                    case XMLStreamConstants.CDATA:
                    case XMLStreamConstants.CHARACTERS: {
                        characters();
                        break;
                    }
                }
            }
        }

    }

    private Object parseStringToPrimitiv(Class clazz, String value) {
        if (Date.class.equals(clazz)) {
            return new Date(Long.parseLong(value));
        } else if (String.class.equals(clazz)) {
            return value;
        } else if (Boolean.class.equals(clazz)) {
            return Boolean.valueOf(value);
        } else if (Integer.class.equals(clazz)) {
            return Integer.valueOf(value);
        } else if (Long.class.equals(clazz)) {
            return Long.valueOf(value);
        } else {
            logger.error("Type not supported!");
            return null;
        }
    }

    private void fill() {

        try {
            object = (Serializable) clazz.newInstance();
            for (Map.Entry<String, Serializable> property : properties.entrySet()) {
                String setterName = "set".concat(property.getKey());
                String getterName = "get".concat(property.getKey());
                Class fieldType = null;
                try {
                    fieldType = clazz.getMethod(getterName, null).getReturnType();
                } catch (NoSuchMethodException | NullPointerException | SecurityException ex) {
                    logger.error("Getter {} not found. Skip.", getterName);
                    continue;
                }
                if (null != fieldType) {
                    try {
                        Method setter = clazz.getMethod(setterName, fieldType);
                        if (String.class.isInstance(property.getValue())
                                && basicClasses.contains(fieldType)) {
                            setter.invoke(object, parseStringToPrimitiv(
                                    fieldType, (String) property.getValue()));
                        } else {
                            setter.invoke(object, property.getValue());
                        }
                    } catch (InvocationTargetException | NoSuchMethodException | SecurityException ex) {
                        logger.error("Setter {} not found. Skip.", setterName);
                    }
                }
            }
        } catch (InstantiationException | IllegalAccessException ex) {
            logger.error("Error creaing object of class: {}", clazz.getCanonicalName());
        }
    }

    private void characters() {
        int start = reader.getTextStart();
        int length = reader.getTextLength();
        String str = (new String(reader.getTextCharacters(), start, length));
        properties.put(currentProp, str);
    }

    private void endElement() {
        currentProp = null;
        accum--;
    }

    private void startElement() {
        accum++;
        String className = reader.getAttributeValue("", "class");
        if (null == className) {
            currentProp = reader.getLocalName();
        } else {
            properties.put(currentProp, (new MyXmlPojoDecoder(reader)).decode());
        }
    }
;

}
