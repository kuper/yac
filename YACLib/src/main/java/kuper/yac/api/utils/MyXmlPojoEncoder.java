package kuper.yac.api.utils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Level;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vkuprin
 */
public class MyXmlPojoEncoder {

    private final static HashSet<Class> basicClasses = new HashSet<>(Arrays.asList(String.class, Boolean.class, Integer.class, Long.class, Date.class));

    private final Logger logger = LoggerFactory.getLogger(MyXmlPojoEncoder.class);

    public boolean encode(XMLStreamWriter writer, Serializable object) {
        synchronized (writer) {
            Map<String, Serializable> properties = new HashMap<>();
            Class classObj = object.getClass();
            String fullClassName = classObj.getCanonicalName();
            String shortClassName = classObj.getSimpleName();
            Method[] methods = classObj.getDeclaredMethods();
            for (Method method : methods) {
                String name = method.getName();
                if (name.startsWith("get")) {
                    try {
                        Object value = method.invoke(object, null);
                        if (value instanceof Serializable) {
                            properties.put(name, (Serializable) value);
                        }
                    } catch (IllegalAccessException ex) {
                        logger.error(ex.getLocalizedMessage());
                    } catch (IllegalArgumentException ex) {
                        logger.error(ex.getLocalizedMessage());
                    } catch (InvocationTargetException ex) {
                        logger.error(ex.getLocalizedMessage());
                    }

                }
            }
            try {
                writer.writeStartElement(shortClassName);
                writer.writeAttribute("class", fullClassName);
                for (Map.Entry<String, Serializable> entry : properties.entrySet()) {
                    Serializable value = entry.getValue();
                    Class propClass = value.getClass();
                    writer.writeStartElement(entry.getKey().substring(3));
                    if (basicClasses.contains(propClass)) {
                        if (propClass.equals(String.class)) {
                            writer.writeCData((String) value);
                        } else if (propClass.equals(Date.class)) {
                            writer.writeCharacters(String.valueOf(((Date) value).getTime()));
                        } else {
                            writer.writeCharacters(value.toString());
                        }
                    } else {
                        this.encode(writer, value);
                    }
                    writer.writeEndElement();
                }
                writer.writeEndElement();
                writer.flush();
            } catch (XMLStreamException ex) {
                java.util.logging.Logger.getLogger(MyXmlPojoEncoder.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        }
    }

}
