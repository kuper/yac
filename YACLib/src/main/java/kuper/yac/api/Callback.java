package kuper.yac.api;

import java.io.Serializable;

/**
 *
 * @author kuper
 *
 */
public interface Callback {
    public void callback(Serializable object);
}
