package kuper.yac.api;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import kuper.yac.api.utils.MyXmlPojoDecoder;
import kuper.yac.api.utils.MyXmlPojoEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vkuprin
 */
public abstract class XMLProtocol extends SimpleProcessor implements Protocol {

    private final Logger logger = LoggerFactory.getLogger(XMLProtocol.class);

    protected XMLStreamReader reader;
    protected XMLStreamWriter writer;

    protected Boolean listen = true;
    
    protected InputStream input;
    protected OutputStream output;
    
    protected final MyXmlPojoEncoder encoder = new MyXmlPojoEncoder();
    
    public InputStream getInput() {
        return input;
    }

    @Override
    public void setInput(InputStream input) {
        this.input = input;
    }

    public OutputStream getOutput() {
        return output;
    }

    @Override
    public void setOutput(OutputStream output) {
        this.output = output;
    }

    @Override
    public boolean isActive() {
        return listen;
    }

    protected void createReader() {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        try {
            this.reader = factory.createXMLStreamReader((input));
        } catch (XMLStreamException ex) {
            throw new RuntimeException("Create XML reader error");
        }
    }


    protected void createWriter() {
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        try {
            this.writer = factory.createXMLStreamWriter(output);
            writer.writeStartDocument("UTF-8", "1.0");
            writer.flush();
        } catch (XMLStreamException ex) {
            throw new RuntimeException("Create XML writer error");
        }
    }

    protected void listen() throws XMLStreamException {
        while (listen && reader.hasNext()) {
            Integer eventType = reader.getEventType();
            if (eventType.equals(XMLStreamConstants.START_ELEMENT)) {
                String className = reader.getAttributeValue("", "class");
                if (null != className) {
                    MyXmlPojoDecoder decoder = new MyXmlPojoDecoder(reader);
                    Serializable dto = decoder.decode();
                    if (null != dto) {
                        processDTO(dto);
                    }
                }
            }
            if (listen) {
                reader.next();
            }
        }
        listen = false;
        logger.debug("listening stopped");
    }
    
    @Override
    abstract public void process();

    
    abstract protected void processDTO(Serializable dto);

}
